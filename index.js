// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name = "Ihor", age = 28, salary = 1500) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        if (newName === '') {
            console.error("Kindly enter an Employee name");
            return;
        }

        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (value > 65) {
            console.error("You need to be retired now");
            return;
        } else if (value < 18) {
            console.error("You are very young for an employee");
            return;
        }

        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (value > 7000) {
            console.error("We can't pay more than 7000");
            return;
        }
        
        this._age = value;
    }
}

let emp1 = new Employee('Max', 20, 2000);
console.log(emp1);

let emp2 = new Employee();
console.log(emp2);

let emp3 = new Employee("Max", 30, 5000);
console.log(emp3);

class Programmer extends Employee {
    constructor(name, age, salary, lang = "Ukraine") {
        super(name, age, salary)
        this._lang = lang
    }

    get salary() {
        return this._salary * 3
    }
    
    set salary(value) {
        this._salary = value
    }
}

let prog1 = new Programmer();
console.log(prog1);

let prog2 = new Programmer("Nick", 20, 2900, "English");
console.log(prog2);

let prog3 = new Programmer("Angela", 65, 3200, "Deuthse");
console.log(prog3);
